var studentsAndPoints = [
  'Алексей Петров',0,
  'Ирина Овчинникова',60,
  'Глеб Стукалов',30,
  'Антон Павлович',30,
  'Виктория Заровская',30,
  'Алексей Левенец',70,
  'Тимур Вамуш',30,
  'Евгений Прочан',60,
  'Александр Малов',0];

var i, next, index = max = 0;

// 1. Вывод списка студентов

function printList()
{
  for (i=0; i<studentsAndPoints.length; i+=2) console.log('Студент %s набрал %d баллов',studentsAndPoints[i],studentsAndPoints[i+1]);
  console.log('\n\n');
}
printList();


// 2. Поиск студента, набравшего максимальное количество баллов

for (i=0; i<studentsAndPoints.length; i+=2)
{
  next = i + 1;
  if (studentsAndPoints[next]>max)
  {
    index = i;
    max = studentsAndPoints[next];
  }
}
console.log('Студент набравший максимальный балл:\nСтудент %s имеет максимальный балл %d\n\n\n',studentsAndPoints[index],max);


// 3. Добавление новый студентов

studentsAndPoints.push('Николай Фролов',0,'Олег Боровой',0);
printList();

// 4. Внесение изменений в массив: добавление баллов студентам

index = studentsAndPoints.indexOf('Антон Павлович');
if (index!==-1) studentsAndPoints[index+1] += 10;
index = studentsAndPoints.indexOf('Николай Фролов');
if (index!==-1) studentsAndPoints[index+1] += 10;
printList();

// 5. Список студентов, не набравших баллов

console.log('Студенты не набравшие баллов:');
for (i=0; i<studentsAndPoints.length; i+=2) if (studentsAndPoints[i+1]==0) console.log(studentsAndPoints[i]);
console.log('\n\n');

// 6. Удаление данных о студентах, набравших 0 баллов

for (i=studentsAndPoints.length-2; i>=0; i-=2) if (studentsAndPoints[i+1]==0) studentsAndPoints.splice(i,2);
printList();